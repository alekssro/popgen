# GWAS Analysis methods

## **GCTA-fastBAT: a fast and flexible set-Based Association Test using GWAS summary data**   

 This method performs a fast set-based association analysis for human complex traits using summary-level data from genome-wide association studies  (GWAS) and linkage disequilibrium (LD) data from a reference sample with individual-level genotypes. Please see [Bakshi et al. (2016 Scientific Reports)](http://www.nature.com/articles/srep32894) for details about the method. This module is developed by Andrew Bakshi and Jian Yang.  

**Note:** most other GCTA options are also valid in this analysis.  

**Examples**  

Gene-based test

```bash
gcta64 --bfile test --maf 0.01 --fastBAT assoc.txt --fastBAT-gene-list gene_list.txt --out test --thread-num 10
```

Segment-based test (size of a segment = 100Kb) 

```bash
gcta64 --bfile test --maf 0.01 --fastBAT assoc.txt --fastBAT-seg 100 --out test --thread-num 10
```

Set-based test with a customized set file (note that this can be used to test all SNPs involved in a pathway)

```bash
gcta64 --bfile test --maf 0.01 --fastBAT assoc.txt --fastBAT-set-list set.txt --out test --thread-num 10
```

**Options**    

`--bfile test` 

Input SNP genotype data (in PLINK binary PED format) as the reference set for LD estimation. For a single-cohort based GWAS, the GWAS cohort itself  can be used as the reference set. For a meta-analysis, you can use one  of the largest participating cohorts as the reference set. If none of  them are available, you might use data from the 1000 Genomes Project  (you will need PLINK2 --vcf option to convert the data into PLINK binary PED format). Please see Figure 1 of Bakshi et al. 2016 for a comparison of results using different reference sets for LD.   

`--fastBAT assoc.txt`

Input association p-values of a list of SNPs. This can be from a standard GWAS or from a meta-analysis. Input file format assoc.txt:

| SNP    | p      |
| ------ | ------ |
| rs1001 | 0.0055 |
| rs1002 | 0.0115 |
| ...    |        |

`--fastBAT-gene-list gene_list.txt`

Input gene list with gene start and end positions. Input file format gene_list.txt (columns are gene ID, chromosome, left and right side boundary of the gene region)

| Chr  | Start | End   | Gene  |
| ---- | ----- | ----- | ----- |
| 1    | 19774 | 19899 | Gene1 |
| 1    | 34627 | 35558 | Gene2 |

`--fastBAT-set-list  set.txt`

Input set list with name and list of SNPs in the set. Input file format set.txt (set ID, followed by SNPs, then END, then blank space before next set):

```
Set1
rs1234534
rs5827743
rs9737542
END

Set2    
rs1252514
……
```

This option provides an opportunity for you to customize your own sets of  SNPs. For example, you can create a SNP set which contains all the 1KGP  SNPs in genes involved in a pathway listed in the file below. pathway list: c2.cp.v5.1.symbols.gmt (downloaded from Broad GSEA)   

`--fastBAT-seg 100`

Perform fastBAT analysis based on segments of size 100Kb (default).  

--------------------------------------------------

# Enrichment of genes

## Gene Ontology (GO Biological process... buscar referencia)

![](/mnt/windowsData/aleks/Documents/Nextcloud/Universidad/Bioinformatics/Second Semester/PopGen/zFinalProyect/popgen/Figures/Enrichr/Ontology_GO_Biological_Process_2017b_bar_graph.png)

## Pathways: WikiPathways 2016 (buscar referencia), KEGG was giving results for diseases.

![](/mnt/windowsData/aleks/Documents/Nextcloud/Universidad/Bioinformatics/Second Semester/PopGen/zFinalProyect/popgen/Figures/Enrichr/WikiPathways_2016_bar_graph.png)

## Cell Types: Human Gene Atlas (buscar referencia)

![](/mnt/windowsData/aleks/Documents/Nextcloud/Universidad/Bioinformatics/Second Semester/PopGen/zFinalProyect/popgen/Figures/Enrichr/CellTypes_Human_Gene_Atlas_bar_graph.png)

## Citation:

Chen EY, Tan CM, Kou Y, Duan Q, Wang Z, Meirelles GV, Clark NR, Ma'ayan A. Enrichr: interactive and collaborative HTML5 gene list enrichment analysis tool. BMC Bioinformatics. 2013;128(14).

Kuleshov MV, Jones MR, Rouillard AD, Fernandez NF, Duan Q, Wang Z, Koplev S, Jenkins SL, Jagodnik KM, Lachmann A, McDermott MG, Monteiro CD, Gundersen GW, Ma'ayan A. Enrichr: a comprehensive gene set enrichment analysis web server 2016 update. Nucleic Acids Research. 2016; gkw377